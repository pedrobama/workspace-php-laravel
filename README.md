# workspace-php-laravel

### INSTALANDO O DOCKER E DOCKER COMPOSE
## ADICIONANDO OS PACOTES
$ sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository  "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs) stable"

$ sudo apt-get update

### INSTALANDO O DOCKER VERSÃO STABLE
$ sudo apt-get -y install docker-ce

### INCIALIZANDO O SERVIÇO DO DOCKER
$ sudo systemctl status docker
$ sudo usermod -aG docker $USER
$ su - $USER
$ id -nG


### ADICIONANDO PACOTES EXTRAS
$ sudo yum install epel-release

### INTALANDO DOCKER COMPOSE
$ sudo curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-`uname -s`-`uname -m` -o
$ sudo chmod +x /usr/local/bin/docker-compose

### INSTALAR GIT

$ sudo apt-get install git

### CLONE NO PROJETO 

$ sudo git clone https://gitlab.com/pedrobama/workspace-php-laravel.git

$ sudo docker-compose up -d

